FROM node:13.10.1-alpine

RUN npm install -g @vue/cli

RUN apk add --no-cache bash
